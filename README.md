# Company Profile Team A

This project is only using static html for learning better about git and html+css.

### Content:

- Home
- Product & Services
- About
- Contact

### Our Teams (Team A)

- Citra Indra Pradita
- Furqon Ahkamy

### How to colaborate

Clone the Project and open with Ms Visual Code

```
git clone https://gitlab.com/indracip/company-profile-team-a.git
```

### Branch Rule

Create Branch Name using format like this example

```
$ git checkout -b features/about
```
